// HTTP Config Headers
var headers = new Headers();
headers.append("Accept", "application/vnd.twitchtv.v5+json");
headers.append("Client-ID", "6cpmafugrw4y90cew60o9vsjs2hdsi");

var params = {
  method: 'GET',
  headers: headers,
  mode: 'cors'
};

console.log('hi!');

var game = document.getElementsByClassName("font-size-5 js-card__info qa-card__info")[0].innerText;
var _displayName = document.getElementsByClassName('cn-bar__displayname')[0].textContent;
var GetChannelID = 'https://api.twitch.tv/kraken/users?login=' + _displayName;
var gamesArray = [];
var _id = '';

// Appel de l'api channels pour obtenir l'identifiant unique
fetch(GetChannelID, params).then(function(r) {
  console.log('Fetch ID');
  return r.json();
}).then(function(d) {
  _id = d.users[0]._id;

  FetchUntilAll(0);

}).catch(function(error) {
});

function FetchUntilAll(offset) {
  console.log('FetchUntilAll');
  fetch('https://api.twitch.tv/kraken/channels/' + _id + '/videos?broadcast_type=archive&limit=100&offset=' + offset, params).then(function(a) {
    return a.json();
    }).then(function(b) {
    gamesArray = _.concat(gamesArray, b.videos);
    if (offset + 100 < b._total) {
      FetchUntilAll(offset + 100);
    } else {
      SortVODS();
      InsertHTML();
    }
  }).catch(function(e) {
    console.log(e);
  });
}

function SortVODS() {
  console.log('SortVODS');
  var gamesReverse = _.reverse(gamesArray);
  gamesArray = _.filter(gamesReverse, { 'game': game});
}

function InsertHTML() {
  console.log('innerHTML');
  var targetHTML = document.getElementsByClassName('cn-tabs__inner js-cn-tabs__inner')[0];
  var selectNode = document.createElement("select");
  selectNode.setAttribute("id", "TVODPlaylist_SELECT");
  for (var i = 0; i < gamesArray.length; i++) {
    var option = document.createElement("option");
    option.setAttribute("value", gamesArray[i].url);
    option.innerHTML = gamesArray[i].title;
    selectNode.appendChild(option);
  }
  targetHTML.parentNode.insertBefore(selectNode, targetHTML.nextSibling);

  selectNode.addEventListener('change', function(e) {
    var selectBox = document.getElementById("TVODPlaylist_SELECT");
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
    window.location.href = selectedValue;
  }, false);
}
