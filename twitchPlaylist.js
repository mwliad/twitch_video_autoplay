/*
  twitchPlaylist.js ajoute une liste des jeux joués parmis les VOD proposées et redirige à la selection vers la première vidéo de ce jeu sur sa chaîne.
*/

// HTTP Config Headers
var headers = new Headers();
headers.append("Accept", "application/vnd.twitchtv.v5+json");
headers.append("Client-ID", "6cpmafugrw4y90cew60o9vsjs2hdsi");

var params = {
  method: 'GET',
  headers: headers,
  mode: 'cors'
};

// Capture du nom du stream pour rechercher son identifiant unique
var _displayName = document.getElementsByClassName('cn-bar__displayname')[0].textContent;
var GetChannelID = 'https://api.twitch.tv/kraken/users?login=' + _displayName;
var gamesArray = [];
var _id = '';

// Appel de l'api channels pour obtenir l'identifiant unique
fetch(GetChannelID, params).then(function(r) {
  return r.json();
}).then(function(d) {
  _id = d.users[0]._id;

  FetchUntilAll(0);

}).catch(function(error) {
});

function FetchUntilAll(offset) {
  fetch('https://api.twitch.tv/kraken/channels/' + _id + '/videos?broadcast_type=archive&limit=100&offset=' + offset, params).then(function(a) {
    return a.json();
    }).then(function(b) {
    gamesArray = _.concat(gamesArray, b.videos);
    if (offset + 100 < b._total) {
      FetchUntilAll(offset + 100);
    } else {
      SortGames();
      InsertHTML();
    }
  }).catch(function(e) {
  });
}

function SortGames() {
  var gamesReverse = _.reverse(gamesArray); // Inverse le tableau pour avoir les vidéos triées de la plus ancienne à la plus récente
  gamesArray = _.uniqBy(gamesReverse, 'game'); // Extrait la première vidéo de chaque jeu
  gamesArray = _.sortBy(gamesArray, ['game']); // Tri les jeux par ordre alphabétique
}

function InsertHTML() {
  var targetHTML = document.getElementsByClassName('filter-bar__left')[0];
  var selectNode = document.createElement("select");
  selectNode.setAttribute("id", "TVODPlaylist_SELECT");
  for (var i = 0; i < gamesArray.length; i++) {
    var option = document.createElement("option");
    option.setAttribute("value", gamesArray[i].url);
    option.innerHTML = gamesArray[i].game;
    selectNode.appendChild(option);
  }
  targetHTML.parentNode.insertBefore(selectNode, targetHTML.nextSibling);

  selectNode.addEventListener('change', function(e) {
    var selectBox = document.getElementById("TVODPlaylist_SELECT");
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
    window.location.href = selectedValue;
  }, false);
}
